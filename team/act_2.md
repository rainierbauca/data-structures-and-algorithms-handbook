# Aspiring Software Engineer

## Christian Visaya

👋 Aspiring Software Engineer! 🚀👨‍💻 — 💌 christianvisaya@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_visaya_christian.jpg](images/bsis_2_visaya_christian.jpg)

### Bio

**Good to know:** I'm a fan of optimization. One thing for sure I've learned throughout my career is that communication and understanding is the most significant part of solving a problem. I prefer clarity over speed.

**Motto:** Stability under pressure

**Languages:** Python, Javascript, PHP

**Other Technologies:** AWS, GCP, Microsoft Azure, Digital Ocean, Alibaba Cloud

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->

# Front End Developer

## Rainier Bauca

😎Front End Developer! 🚀👨‍💻 — 💌 rainierbauca@student.laverdad.edu.ph — SanSimon, Pampanga

![alt act_2_bauca_rainier.jpg](images/act_2_bauca_rainier.jpg)

### Bio

**Good to know:** A dedicated K-Pop fan on a journey to become a developer, blending the love for music with the art of coding and Video editing..🍭🍭

**Motto:** You Only Live Once🍭🍭

**Languages:** Python

**Other Technologies:** Adobe

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/40495ae70faae)


<!-- END -->
